using MairieParis.Business;
using MairieParis.Business.Core;
using MairieParis.Business.Jobs;
using System;
using System.Collections.Generic;
using Xunit;

namespace MairieParis.Tests
{
    public class Tests
    {
        [Fact]
        public void TestConvertToCSV()
        {
            List<CSVLine> lines = new List<CSVLine>() {
                new CSVLine(new List<string>() { "Line1Column1", "Line1Column2" }),
                new CSVLine(new List<string>() { "Line2Column1", "Line2Column2" }),
            };
            Assert.Equal("Line1Column1;Line1Column2\nLine2Column1;Line2Column2\n", CSVUtility.ConvertToCsv(lines));
        }

        [Fact]
        public void FileExistsTest()
        {
            XmlReaderJob job = new XmlReaderJob("../../../XMLFilesTests/PeopleTest.xml");
            Assert.True(job.FileExists());
        }

        [Fact]
        public void SerializeXmlTest()
        {
            XmlReaderJob job = new XmlReaderJob("../../../XMLFilesTests/PeopleTest.xml");
            XMLTest test = job.ReadXmlFile<XMLTest>();

            Assert.Equal("Benjamin", test.firstname);
            Assert.Equal("Spital", test.lastname);
            Assert.Equal("benjamin.spital@epsi.fr", test.email);
        }
    }
}
