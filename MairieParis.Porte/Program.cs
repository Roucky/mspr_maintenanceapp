﻿using System;
using System.IO;
using MairieParis.Business;
using MairieParis.Business.Jobs;
using Microsoft.VisualBasic;

namespace MairieParis.Porte
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                if (!Directory.Exists(Constant.PorteFolder))
                {
                    Console.WriteLine(string.Format("Folder {0} does not exist!", Constant.PorteFolder.ToString()));
                    break;
                }
                XmlReaderJob job;
                foreach (string file in Directory.GetFiles(Constant.PorteFolder))
                {
                    if (Path.GetExtension(file) == ".xml" && file.Contains("Request"))
                    {
                        Console.WriteLine("Manage " + file);
                        job = new XmlReaderJob(file);
                        job.Execute();
                    }
                }
            }
        }
    }
}
