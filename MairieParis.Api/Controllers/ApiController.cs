﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MairieParis.Business;
using MairieParis.Business.Core;
using MairieParis.Business.Database;
using MairieParis.Business.Jobs;
using Microsoft.AspNetCore.Mvc;
using MySqlX.XDevAPI.Common;

namespace MairieParis.Api.Controllers
{
    public class ApiController : Controller
    {
        [HttpGet]
        [Route("espaces/travaux")]
        public string Travaux()
        {
            string ret = string.Empty;
            foreach (Chantier chantier in DatabaseCustom.Chantiers.Where(d => d.Taches.Count(d => d.Etat == Etat.EnCours) > 0).ToList())
            {
                Tache tache = chantier.Taches.Where(d => d.Etat == Etat.EnCours).OrderByDescending(d => d.DateFin).FirstOrDefault();
                ret += string.Format("{0};{1};{2}\n", chantier.Nom, chantier.Adresse, tache.DateFin.ToShortDateString());
            }
            return ret;
        }
        [HttpGet]
        [Route("espaces/ouverts")]
        public string Ouverts()
        {
            string ret = string.Empty;
            foreach (Chantier chantier in DatabaseCustom.Chantiers.Where(d => d.Taches.Count(d => d.Etat == Etat.EnCours) == 0).ToList())
            {
                ret += string.Format("{0};{1}\n", chantier.Nom, chantier.Adresse);
            }
            return ret;
        }

        [HttpGet]
        [Route("eclairage/horaires")]
        public string Eclairage(string date)
        {
            DateTime dateFormatted;
            try
            {
                dateFormatted = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                Response.StatusCode = 404;
                return "ERROR";
            }
            List<Arrondissement> arrondissements = DatabaseCustom.Arrondissements.Distinct().ToList();
            string ret = string.Empty;
            foreach (Lampadaire lampadaire in DatabaseCustom.Lampadaires)
            {
                Plages plage = lampadaire.Arrondissement.Plages.First(d => d.Date == dateFormatted);
                if (plage != null)
                    ret += string.Format("{0};{1};{2};{3}\n", lampadaire.Ltd, lampadaire.Lgtd, plage.PlageDebut, plage.PlageFin);
            }
            return ret;
        }

        [HttpGet]
        [Route("porte/horaires")]
        public async Task<string> Horaires(string codeInstitution, string date)
        {
            DateTime dateFormatted;
            try
            {
                dateFormatted = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                return e.Message;
            }
            Dictionary<string, TimeSpan> idsHoraire = new Dictionary<string, TimeSpan>();
            for (TimeSpan time = new TimeSpan(0, 0, 0); time < new TimeSpan(23, 59, 0); time += new TimeSpan(0, 30, 0))
            {
                string id = Guid.NewGuid().ToString();
                idsHoraire.Add(id, time);
                await CreateXmlFileForTime(codeInstitution, dateFormatted, time, id);
            }
            Thread.Sleep(3000);
            return PorteXmlHorairesToCSV(await GetHoraires(idsHoraire));
        }

        private string PorteXmlHorairesToCSV(List<TimeSpan> horaires)
        {
            string content = string.Empty;
            int i = 1;
            foreach (TimeSpan horaire in horaires)
            {
                content += horaire.ToString(@"hh\:mm");
                if (horaire != horaires.Last())
                {
                    if (i % 2 == 0) content += "\n";
                    else content += ";";
                }
                i++;
            }
            return content;
        }

        private async Task<List<TimeSpan>> GetHoraires(Dictionary<string, TimeSpan> idsHoraire)
        {
            List<TimeSpan> list = new List<TimeSpan>();
            int count = 0;
            foreach (KeyValuePair<string, TimeSpan> id in idsHoraire)
            {
                string filename = Path.Combine(Constant.PorteFolder, string.Format("Response_{0}.xml", id.Key));
                if (System.IO.File.Exists(filename))
                {
                    XmlReaderJob job = new XmlReaderJob(filename);
                    Document document = await job.ReadFile();
                    if (document != null)
                    {
                        if (document.Res.Ouvert)
                        {
                            count++;
                            list.Add(id.Value);
                        }
                        else
                        {
                            if (count >= 1)
                            {
                                list.RemoveRange(list.Count - count + 1, count - 1);
                                list.Add(id.Value);
                            }
                            count = 0;
                        }
                    }

                }
            }
            return list;
        }

        private async Task CreateXmlFileForTime(string codeInstitution, DateTime date, TimeSpan time, string id)
        {
            XmlWriterJob job = new XmlWriterJob();
            job.Execute(new Document()
            {
                Req = new Req()
                {
                    Institution = new Institution()
                    {
                        Code = codeInstitution
                    },
                    Plage = new Plage()
                    {
                        Horaire = time.ToString(@"hh\:mm"),
                        Jour = date
                    },
                    UniqueId = id
                }
            });
        }
    }
}
