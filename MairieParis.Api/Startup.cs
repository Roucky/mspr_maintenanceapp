using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MairieParis.Business.Core;
using MairieParis.Business.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MairieParis.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            RegisterAllFiles();
        }

        private void RegisterAllFiles()
        {
            RegisterLampadaires();
            RegisterArrondissements();

            RegisterChantiers();
        }

        private void RegisterChantiers()
        {
            DatabaseCustom.Chantiers.AddRange(new List<Chantier>()
            {
                new Chantier()
                {
                    Adresse = "10 rue du pélican, 75001 Paris",
                    Nom = "Rue du pélican",
                    ChantierCode = "RUE_PELC",
                    Taches = new List<Tache>()
                    {
                        new Tache()
                        {
                            ChantierCode = "RUE_PELC",
                            Nom = "Terrassement",
                            DateFin = new DateTime(1995, 02, 16),
                            Etat = Etat.Termine
                        },
                        new Tache()
                        {
                            ChantierCode = "RUE_PELC",
                            Nom = "Réfection pavé",
                            DateFin = new DateTime(1999, 08, 10),
                            Etat = Etat.Termine
                        },
                    }
                },
                new Chantier()
                {
                    Adresse = "Plaque Télécom, Port Royal 74014 PARIS",
                    Nom = "Salle Z",
                    ChantierCode = "SLL_Z",
                    Taches = new List<Tache>()
                    {
                        new Tache()
                        {
                            ChantierCode = "SLL_Z",
                            Nom = "Déblaiement",
                            DateFin = new DateTime(1960, 02, 16),
                            Etat = Etat.Termine
                        },
                        new Tache()
                        {
                            ChantierCode = "SLL_Z",
                            Nom = "Évacuation caddie",
                            DateFin = new DateTime(2009, 11, 06),
                            Etat = Etat.Termine
                        },
                        new Tache()
                        {
                            ChantierCode = "SLL_Z",
                            Nom = "Évacuation vélib",
                            DateFin = new DateTime(2008, 07, 31),
                            Etat = Etat.EnCours
                        }
                    }
                },
            });
        }

        private void RegisterArrondissements()
        {
            DatabaseCustom.Arrondissements.AddRange(new List<Arrondissement>()
            {
                new Arrondissement() {
                Code = "01",
                Lampadaires = DatabaseCustom.Lampadaires.Where(d => d.ArrondissementCode == "01").ToList(),
                    Plages = new List<Plages>()
                    {
                        new Plages()
                        {
                            Date = new DateTime(2021,02,05),
                            PlageDebut = new TimeSpan(08, 32, 00),
                            PlageFin = new TimeSpan(20, 12, 00)
                        },
                        new Plages()
                        {
                            Date = new DateTime(2021,02,06),
                            PlageDebut = new TimeSpan(08, 33, 00),
                            PlageFin = new TimeSpan(20, 14, 00)
                        },
                        new Plages()
                        {
                            Date = new DateTime(2021,02,07),
                            PlageDebut = new TimeSpan(08, 37, 00),
                            PlageFin = new TimeSpan(20, 16, 00)
                        },
                    }
                },
                new Arrondissement() {
                Code = "04",
                Lampadaires = DatabaseCustom.Lampadaires.Where(d => d.ArrondissementCode == "04").ToList(),
                    Plages = new List<Plages>()
                    {
                        new Plages()
                        {
                            Date = new DateTime(2021,02,05),
                            PlageDebut = new TimeSpan(08, 33, 00),
                            PlageFin = new TimeSpan(20, 09, 00)
                        },
                        new Plages()
                        {
                            Date = new DateTime(2021,02,06),
                            PlageDebut = new TimeSpan(08, 30, 00),
                            PlageFin = new TimeSpan(20, 01, 00)
                        },
                        new Plages()
                        {
                            Date = new DateTime(2021,02,07),
                            PlageDebut = new TimeSpan(08, 41, 00),
                            PlageFin = new TimeSpan(20, 17, 00)
                        },
                    }
                }
            });
        }

        private void RegisterLampadaires()
        {
            DatabaseCustom.Lampadaires.AddRange(new List<Lampadaire>()
            {
                new Lampadaire()
                {
                    Id = "CD0X+32",
                    ArrondissementCode = "01",
                    Ltd = "-5.1",
                    Lgtd = "-68.3",
                },
                new Lampadaire()
                {
                    Id = "CM1P-t2",
                    ArrondissementCode = "04",
                    Ltd = "-5.1",
                    Lgtd = "-68.3",
                },
                new Lampadaire()
                {
                    Id = "AT43+V2",
                    ArrondissementCode = "04",
                    Ltd = "-23.1",
                    Lgtd = "8.8",
                },
            });
        }
    }
}
