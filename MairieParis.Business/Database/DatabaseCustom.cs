﻿using System;
using System.Collections.Generic;
using MairieParis.Business.Core;

namespace MairieParis.Business.Database
{
    public static class DatabaseCustom
    {
        public static List<Arrondissement> Arrondissements { get; set; } = new List<Arrondissement>();

        public static List<Lampadaire> Lampadaires { get; set; } = new List<Lampadaire>();

        public static List<Chantier> Chantiers { get; set; } = new List<Chantier>();
    }
}
