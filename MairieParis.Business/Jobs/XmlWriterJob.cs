﻿using System;
using System.IO;
using System.Xml.Serialization;
using MairieParis.Business.Core;

namespace MairieParis.Business.Jobs
{
    public class XmlWriterJob
    {
        public XmlWriterJob()
        {
        }

        public void Execute(Document document)
        {
            string filename = Path.Combine(Constant.PorteFolder, string.Format("{0}_{1}.xml", document.Req == null ? "Response" : "Request", document.Req?.UniqueId ?? document.Res?.UniqueId));
            XmlSerializer writer = new XmlSerializer(typeof(Document));
            FileStream file = File.Create(filename);
            writer.Serialize(file, document);
            file.Close();
        }
    }
}
