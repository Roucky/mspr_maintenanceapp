﻿using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using MairieParis.Business.Core;

namespace MairieParis.Business.Jobs
{
    public class XmlReaderJob
    {
        private string filePath;
        private Document porteXmlRequest;

        public XmlReaderJob(string filePath)
        {
            this.filePath = filePath;
        }

        public async void Execute()
        {
            await ReadFile();
            GenerateResponse();
        }

        public async Task<Document> ReadFile()
        {
            XmlSerializer oSerializer = new XmlSerializer(typeof(Document));
            try
            {
                using (Stream oReader = new FileStream(filePath, FileMode.Open))
                {
                    porteXmlRequest = (Document)oSerializer.Deserialize(oReader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(filePath + " => " + e.Message);
            }
            return porteXmlRequest;
        }

        public bool FileExists() => File.Exists(filePath);

        public T ReadXmlFile<T>()
        {
            T ret = default(T);

            if (!FileExists()) return default(T);
            XmlSerializer oSerializer = new XmlSerializer(typeof(T));
            try
            {
                using (Stream oReader = new FileStream(filePath, FileMode.Open))
                {
                    ret = (T)oSerializer.Deserialize(oReader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(filePath + " => " + e.Message);
            }
            return ret;
        }

        public void GenerateResponse()
        {
            Console.WriteLine("Generate Response");
            Random rand = new Random();
            XmlWriterJob job = new XmlWriterJob();
            if (porteXmlRequest != null)
            {
                TimeSpan time = TimeSpan.ParseExact(porteXmlRequest.Req.Plage.Horaire, @"hh\:mm", CultureInfo.InvariantCulture);
                job.Execute(new Document()
                {
                    Res = new Res()
                    {
                        UniqueId = porteXmlRequest.Req.UniqueId,
                        Ouvert = (time >= new TimeSpan(8, 0, 0) && time < new TimeSpan(12, 0, 0)) || (time >= new TimeSpan(14, 0, 0) && time < new TimeSpan(18, 0, 0))
                    }
                });
            }
            File.Delete(filePath);
        }
    }
}
