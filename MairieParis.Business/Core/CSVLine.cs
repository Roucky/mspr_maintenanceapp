﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MairieParis.Business.Core
{
    public class CSVLine
    {
        public List<string> Columns { get; set; }

        public CSVLine(List<string> columns)
        {
            this.Columns = columns;
        }
    }
}
