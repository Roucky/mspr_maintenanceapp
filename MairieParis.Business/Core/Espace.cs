﻿using System;
using System.Collections.Generic;
using System.Linq;
using MairieParis.Business.Database;

namespace MairieParis.Business.Core
{
    public class Chantier
    {
        public string Nom { get; set; }

        public string ChantierCode { get; set; }

        public string Adresse { get; set; }

        public List<Tache> Taches { get; set; } = new List<Tache>();
    }

    public class Tache
    {
        public string ChantierCode { get; set; }

        public Chantier Chantier
        {
            get => DatabaseCustom.Chantiers.FirstOrDefault(d => d.ChantierCode == ChantierCode);
        }

        public string Nom { get; set; }

        public DateTime DateFin { get; set; }

        public Etat Etat { get; set; }
    }

    public enum Etat
    {
        EnCours,
        Termine
    }
}
