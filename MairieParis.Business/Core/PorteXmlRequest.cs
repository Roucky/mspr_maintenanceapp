﻿using System;
using System.Collections.Generic;

namespace MairieParis.Business.Core
{
    public class Document
    {
        public Req Req { get; set; }

        public Res Res { get; set; }

        public Ctry Ctry { get; set; }

        public string Id { get; set; }

        public List<Lpdre> Lpdre { get; set; }

        public List<Arrdt> Arrdt { get; set; }
    }

    public class Lpdre
    {
        public string Id { get; set; }

        public string Ltd { get; set; }

        public string Lgtd { get; set; }
    }

    public class Req 
    {
        public string UniqueId { get; set; }

        public Institution Institution { get; set; }

        public Plage Plage { get; set; }
    }

    public class Res
    {
        public string UniqueId { get; set; }

        public bool Ouvert { get; set; }
    }

    public class Institution
    {
        public string Code { get; set; }
    }

    public class Plage
    {
        public DateTime Jour { get; set; }

        public string Horaire { get; set; }
    }
}
