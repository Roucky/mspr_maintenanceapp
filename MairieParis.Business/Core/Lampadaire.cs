﻿using System;
using System.Collections.Generic;
using System.Linq;
using MairieParis.Business.Database;

namespace MairieParis.Business.Core
{
    public class Lampadaire
    {
        public string Id { get; set; }

        public string Ltd { get; set; }

        public string Lgtd { get; set; }

        public string ArrondissementCode { get; set; }

        public Arrondissement Arrondissement
        {
            get => DatabaseCustom.Arrondissements.FirstOrDefault(d => d.Code == this.ArrondissementCode);
        }
    }

    public class Plages
    {
        public TimeSpan PlageFin { get; set; }

        public TimeSpan PlageDebut { get; set; }

        public DateTime Date { get; set; }
    }
}
