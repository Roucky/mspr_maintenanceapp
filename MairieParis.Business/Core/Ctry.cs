﻿using System.Collections.Generic;

namespace MairieParis.Business.Core
{
    public class Eclairage
    {
        public string Code { get; set; }
    }

    public class Ctry : Eclairage
    {
        public Rg Rg { get; set; }
    }

    public class Rg : Eclairage
    {
        public Dpt Dpt { get; set; }
    }

    public class Dpt : Eclairage
    {
        public Arrdt Arrdt { get; set; }
    }

    public class Arrdt
    {
        public string Id { get; set; }

        public List<Plge> Plge { get; set; }
    }

    public class Plge
    {
        public string Fn { get; set; }

        public string Db { get; set; }
    }
}