﻿using System;
using System.Collections.Generic;

namespace MairieParis.Business.Core
{
    public class Arrondissement
    {
        public string Code { get; set; }

        public List<Lampadaire> Lampadaires { get; set; }

        public List<Plages> Plages { get; set; }
    }
}
