﻿using MairieParis.Business.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace MairieParis.Business
{
    public class CSVUtility
    {
        public static string ConvertToCsv(List<CSVLine> lines)
        {
            string ret = string.Empty;
            foreach (CSVLine line in lines)
            {
                ret += string.Format("{0}\n", string.Join(";", line.Columns));
            }
            return ret;
        }
    }
}
